#ifndef __FRAMEWORK_ASSETS_ANIMATION_JOINTVERTEXMAPPING_H_INCLUDED__
#define __FRAMEWORK_ASSETS_ANIMATION_JOINTVERTEXMAPPING_H_INCLUDED__

#include "../../common.h"

struct JointVertexMapping
{
	int32_t jointIndex;
	float weight;
};

#endif
